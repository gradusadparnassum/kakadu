module kakadu.reader;
import kakadu.streams;

class Token {
  private long myColumn;
  private long myLine;
  this(long line, long column) {
    myColumn = column;
    myLine = line;
  }

  long line() {
    return myLine;
  }

  long column() {
    return myColumn;
  }
}

class SemiColon : Token {
  this(long line, long column) {
    super(line, column);
  }
}

class Identifier : Token {
  private string myText;
  this(long line, long column, string text) {
    myText = text;
    super(line, column);
  }
}

class Reader : IStream!Token {
  private IStream!char myInput;

  static Reader fromString(string s) {
    return new Reader(new StringStream(s));
  }

  this(IStream!char input) {
    myInput = input;
  }

  Token next() {
    return new SemiColon(0, 0);
  }

  bool atEnd() {
    return true;
  }

  unittest {
    auto r = Reader.fromString(";;");
    auto token = r.next();
    assert(token.line() == 0);
    assert(token.column() == 0);
    auto token2 = r.next();
    assert(token2.line() == 0);
    assert(token2.column() == 1);
  }
}
