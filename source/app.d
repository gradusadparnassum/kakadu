import std.stdio;
import kakadu.streams;
import kakadu.reader;

void main() {
  auto s = new StringStream("abcd");
  auto r = new Reader(s);
  r.next();

  writeln("Edit source/app.d to start your project.");
}
