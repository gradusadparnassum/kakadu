#include <assert.h>
#include <kakadu/core.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void eval_file(char *);

static char *HELP_TEXT = ""
			 "kakadu\n"
			 "Usage: kakadusourcefile";

static char *
triReadExpression(char *path, char*content)
{
	assert(false);
}


static char *
tbyReadCompleteFile(char * path)
{
	assert(false);
}


int
main(int argc, char *argv[])
{
	int option;
	while ((option = getopt(argc, argv, ":hf:")) != -1) {
		switch (option) {
		case 'h':
			puts(HELP_TEXT);
			return -1;
		case 'f':
			printf("Given File: %s\n", optarg);
			break;
		case ':':
			printf("option needs a value\n");
			break;
		case '?':
			printf("unknown option: %c\n", optopt);
			break;
		}
	}
	if (optind < argc) {
		eval_file(argv[optind]);
	} else {
		goto FAIL_NO_SOURCE_FILE;
	}
	return 0;

FAIL_NO_SOURCE_FILE:
	fputs("problem, no source file given", stderr);
	return -1;
}

void
eval_file(char *path)
{
	assert(path != NULL);
	assert(strlen(path) > 0);
	printf("Eval file %s\n", path);
	char *content = tbyReadCompleteFile(path);
	triReadExpression(path, content);
	free(content);
}
